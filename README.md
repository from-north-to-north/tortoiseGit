# 使用tortoiseGit图形化工具管理gitte仓库
以前对gitte的操作也就是向上面的OpenHarmony仓库提一些轻量级pr。现在要经常向自己的gitte仓库上传代码，但是笔者不想记那些git命令。于是网上冲浪，找了一个windows平台tortoiseGit图形化工具管理gitte仓库的代码。

# 1.使用环境搭建

## 1.1 安装TortoiseGit
   
- 首先先在windows上安装git，下载地址：https://gitforwindows.org/

- 再下载tortoiseGit安装包和tortoiseGit简体中文的程序包
   - TortoiseGit[下载地址](https://tortoisegit.org/download/)

![tortoiseGit](media/tortoiseGit.png)

## 1.2 配置本地git库和远程git库

1、首先创建一个密钥，打开安装好的PuTTYgen
![PuTTYgen](media/PuTTYgen.png)

2、点击生成ssh密钥，然后点击save private key保存私钥文件在本地电脑(后边会有指定私钥的一步,指定私钥文件地址之后就不要再移动私钥文件,不然git保存和拉取代码就会失败)

![密钥生成](media/miyao.png)

3、在gitee添加刚刚生成的密钥
![在gitee添加刚刚生成的密钥](media/gitee.png)

# 2.几个使用场景

### 2.1 在本地管理一个新建的gitee仓库

1、点击gitee图标新建一个仓库

![点击gitee图标新建一个仓库](media/gitee1.png)

2、建好之后，点击克隆，复制ssh的url

![点击克隆](media/gitee2.png)

3、在本地新建一个文件夹（笔者一般使用仓库的名称），右击鼠标选择`Git在这里创建一个版本库`
![新建一个文件夹](media/gitee3.png)

此时该文件夹下就会出现一个.git文件夹

![此时该文件夹下就会出现一个.git文件夹](media/gitee4.png)

4、此时回到上一步新建的文件夹，右击>tortoiseGit>设置

![右击](media/gitee5.png)

5、填写刚刚复制的码云上新建项目的ssh地址就行 > 导入PuTTYgen生成的私钥文件 > 点击添加保存

![点击添加保存](media/gitee6.png)

6、上传本地仓库到远端
![上传本地仓库到远端](media/gitee7.png)

![上传本地仓库到远端](media/gitee8.png)

必须是显示`成功`才行
![](media/gitee9.png)

### 2.2 在远端(网页)的gitee上修改代码后需要将代码同步到本地(个人电脑)

1、在远端(网页)的gitee仓库修改仓库内容

![输入图片说明](media/gitee10.png)

2、再本地右击`git同步`

![](media/gitee11.png)

点击拉取，会显示本地仓库和远端仓库的不同
![](media/gitee12.png)

再点击一次拉取即可同步代码
![](media/gitee13.png)

### 2.3 修改上一次提交至gitee仓库的代码

1.如果想修改上一次提交的内容，在本地仓库修改好内容，提价到远端时需要勾选`修改上一次提交`

![](media/gitee14.png)

2.点击`提交并且推送`后,勾选`强制覆盖已知`或者`强制覆盖所有`

![](media/gitee15.png)


